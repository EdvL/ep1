#include "eleitor.hpp"
Eleitor::Eleitor(string nome, string votoP, string votoS, string votoG, string votoDF, string votoDE)
{
	set_nome(nome);
	set_votoP(votoP);
	set_votoS(votoS);
	set_votoG(votoG);
	set_votoDF(votoDF);
	set_votoDE(votoDE);
}
Eleitor::~Eleitor() {}

string Eleitor::get_nome()
{
	return nome;
}
void Eleitor::set_nome(string nome)
{
	this->nome = nome;
}
string Eleitor::get_votoP()
{
	return votoP;
}
void Eleitor::set_votoP(string votoP)
{
	this->votoP = votoP;
}
string Eleitor::get_votoS()
{
	return votoS;
}
void Eleitor::set_votoS(string votoS)
{
	this->votoS = votoS;
}
string Eleitor::get_votoG()
{
	return votoG;
}
void Eleitor::set_votoG(string votoG)
{
	this->votoG = votoG;
}
string Eleitor::get_votoDF()
{
	return votoDF;
}
void Eleitor::set_votoDF(string votoDF)
{
	this->votoDF = votoDF;
}
string Eleitor::get_votoDE()
{
	return votoDE;
}
void Eleitor::set_votoDE(string votoDE)
{
	this->votoDE = votoDE;
}
