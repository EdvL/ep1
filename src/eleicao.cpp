#include "eleicao.hpp"
#include <vector>
#include <iostream>
#include <fstream>
#include "candidato.hpp"
#include "eleitor.hpp"
using namespace std;

void Eleicao::readDep()
{
        ifstream dfile;
        votoV = 0;
        dfile.open("data/Cand_edit_DF.csv");

        if (dfile.is_open())
        {
                while (dfile)
                {
                        getline(dfile, this->cargoV, ',');
                        getline(dfile, this->codigoV, ',');
                        getline(dfile, this->nomeurnaV, ',');
                        getline(dfile, this->partidoV, '\n');
                        deputadoVector.push_back(new Candidato(cargoV, codigoV, nomeurnaV, partidoV, votoV));
                }
        }
        else
        {
                cout << "Erro ao abrir arquivo" << endl;
        }
        dfile.close();
}
void Eleicao::readCand()
{

        ifstream pfile;
        votoV = 0;
        pfile.open("data/cand_2018_BR.csv");

        if (pfile.is_open())
        {
                while (pfile)
                {
                        getline(pfile, this->cargoV, ',');
                        getline(pfile, this->codigoV, ',');
                        getline(pfile, this->nomeurnaV, ',');
                        getline(pfile, this->partidoV, '\n');
                        candidatoVector.push_back(new Candidato(cargoV, codigoV, nomeurnaV, partidoV, votoV));
                }
        }
        else
        {
                cout << "Erro ao abrir arquivo" << endl;
        }
        pfile.close();
}
void Eleicao::infoCandidato(string num_candidato, string cargo_cand)
{
        cout << "\n \n"
             << endl;
             for(unsigned int c=0;c<candidatoVector.size();c++){
                     if(num_candidato == candidatoVector[c]->get_codigo()){
                             goto ExistP;
                             break;
                     }
             }
             cout << "Candidato inexistente" << endl;
        ExistP:if (cargo_cand == "PRESIDENTE")
        {
                for (unsigned int aux = 0; aux < candidatoVector.size(); aux++)
                {
                        if (num_candidato == candidatoVector[aux]->get_codigo() && cargo_cand == candidatoVector[aux]->get_cargo())
                        {
                                cout << "Nome: " << candidatoVector[aux]->get_nomeurna() << endl;
                                cout << "Numero: " << candidatoVector[aux]->get_codigo() << endl;
                                cout << "Partido: " << candidatoVector[aux]->get_partido() << endl;
                        }
                }
                for (unsigned int aux = 0; aux < candidatoVector.size(); aux++)
                {
                        if (num_candidato == candidatoVector[aux]->get_codigo() && candidatoVector[aux]->get_cargo() == "VICE-PRESIDENTE")
                        {
                                cout << "Vice: " << candidatoVector[aux]->get_nomeurna() << endl;
                        }
                }
        }
}
//==================================================================================================
void Eleicao::infoDep(string num_dep, string cargo_dep)
{
        cout << "\n"
             << endl;
        cout << "\n"
             << endl;
             for(unsigned int c=0;c<deputadoVector.size();c++){
                     if(num_dep == deputadoVector[c]->get_codigo()){
                             goto Exist;
                             break;
                     }
             }
             cout << "Candidato inexistente" << endl;
             cout << "Por favor, use a função corrigir" << endl;
        Exist:for (unsigned int fon = 0; fon < deputadoVector.size(); fon++)
        {

                if (num_dep == deputadoVector[fon]->get_codigo() && cargo_dep == deputadoVector[fon]->get_cargo())
                {
                        cout << "Nome: " << deputadoVector[fon]->get_nomeurna() << endl;
                        cout << "Numero: " << deputadoVector[fon]->get_codigo() << endl;
                        cout << "Partido: " << deputadoVector[fon]->get_partido() << endl;
                }
        }
        for (unsigned int fon = 0; fon < deputadoVector.size(); fon++)
        {
                if (num_dep == deputadoVector[fon]->get_codigo() && deputadoVector[fon]->get_cargo() == "1 SUPLENTE")
                {

                        cout << "1º SUPLENTE: " << deputadoVector[fon]->get_nomeurna() << endl;
                        ;
                }
                if (num_dep == deputadoVector[fon]->get_codigo() && deputadoVector[fon]->get_cargo() == "2 SUPLENTE")
                {
                        cout << "2º SUPLENTE: " << deputadoVector[fon]->get_nomeurna() << endl;
                        ;
                }
                if (num_dep == deputadoVector[fon]->get_codigo() && deputadoVector[fon]->get_cargo() == "VICE-GOVERNADOR")
                {
                        cout << "Vice: " << deputadoVector[fon]->get_nomeurna() << endl;
                        ;
                }
        }
}

void Eleicao::Apuracao(vector<Eleitor *> &vetor)
{
        int contador = 0;
        int cs = 0;
        int cg = 0;
        int cdf = 0;
        int cdd = 0;
        int unsigned vencedor = 0;
        int unsigned vs = 0;
        int unsigned vg = 0;
        int unsigned vdf = 0;
        int unsigned vdd = 0;
        string cargoE = "x";
        string codigoE = "x";
        string nomeurnaE = "Empate";
        string partidoE = "x";
        int votoE = 0;
        candidatoVector.push_back(new Candidato(cargoE, codigoE, nomeurnaE, partidoE, votoE));
        deputadoVector.push_back(new Candidato(cargoE, codigoE, nomeurnaE, partidoE, votoE));

        for (unsigned int i = 0; i < vetor.size(); i++)
        {
                for (unsigned int c = 0; c < candidatoVector.size(); c++)
                {

                        if (candidatoVector[c]->get_codigo() == vetor[i]->get_votoP() && candidatoVector[c]->get_cargo() == "PRESIDENTE")
                        {
                                candidatoVector[c]->set_voto(candidatoVector[c]->get_voto() + 1);
                                vetor[i]->set_votoP(candidatoVector[c]->get_nomeurna());
                        }
                }
        }
        for (unsigned int aux = 0; aux < vetor.size(); aux++)
        {
                for (unsigned int c = 0; c < deputadoVector.size(); c++)
                {

                        if (deputadoVector[c]->get_codigo() == vetor[aux]->get_votoS() && deputadoVector[c]->get_cargo() == "SENADOR")
                        {
                                deputadoVector[c]->set_voto(deputadoVector[c]->get_voto() + 1);
                                vetor[aux]->set_votoS(deputadoVector[c]->get_nomeurna());
                        }
                        if (deputadoVector[c]->get_codigo() == vetor[aux]->get_votoG() && deputadoVector[c]->get_cargo() == "GOVERNADOR")
                        {
                                deputadoVector[c]->set_voto(deputadoVector[c]->get_voto() + 1);
                                vetor[aux]->set_votoG(deputadoVector[c]->get_nomeurna());
                        }
                        if (deputadoVector[c]->get_codigo() == vetor[aux]->get_votoDF() && deputadoVector[c]->get_cargo() == "DEPUTADO FEDERAL")
                        {
                                deputadoVector[c]->set_voto(deputadoVector[c]->get_voto() + 1);
                                vetor[aux]->set_votoDF(deputadoVector[c]->get_nomeurna());
                        }
                        if (deputadoVector[c]->get_codigo() == vetor[aux]->get_votoDE() && deputadoVector[c]->get_cargo() == "DEPUTADO DISTRITAL")
                        {
                                deputadoVector[c]->set_voto(deputadoVector[c]->get_voto() + 1);
                                vetor[aux]->set_votoDE(deputadoVector[c]->get_nomeurna());
                        }
                }
        }

        for (unsigned int i = 0; i < candidatoVector.size(); i++)
        {
                if (candidatoVector[i]->get_voto() > contador)
                {
                        contador = candidatoVector[i]->get_voto();
                        vencedor = i;
                }
        }
        for (unsigned int a = 0; a < deputadoVector.size(); a++)
        {
                if (deputadoVector[a]->get_voto() > cs && deputadoVector[a]->get_cargo() == "SENADOR")
                {
                        cs = deputadoVector[a]->get_voto();
                        vs = a;
                }
        }
        for (unsigned int b = 0; b < deputadoVector.size(); b++)
        {
                if (deputadoVector[b]->get_voto() > cg && deputadoVector[b]->get_cargo() == "GOVERNADOR")
                {
                        cg = deputadoVector[b]->get_voto();
                        vg = b;
                }
        }
        for (unsigned int d = 0; d < deputadoVector.size(); d++)
        {
                if (deputadoVector[d]->get_voto() > cdf && deputadoVector[d]->get_cargo() == "DEPUTADO FEDERAL")
                {
                        cdf = deputadoVector[d]->get_voto();
                        vdf = d;
                }
        }
        for (unsigned int e = 0; e < deputadoVector.size(); e++)
        {
                if (deputadoVector[e]->get_voto() > cdd && deputadoVector[e]->get_cargo() == "DEPUTADO DISTRITAL")
                {
                        cdd = deputadoVector[e]->get_voto();
                        vdd = e;
                }
        }
        for (unsigned int i = 0; i < deputadoVector.size(); i++)
        {
                if (deputadoVector[vdd]->get_voto() == deputadoVector[i]->get_voto() && deputadoVector[i]->get_cargo() == "DEPUTADO DISTRITAL" && i != vdd)
                {
                        vdd = deputadoVector.size() - 1;
                }
        }

        for (unsigned int i = 0; i < deputadoVector.size(); i++)
        {
                if (deputadoVector[vdf]->get_voto() == deputadoVector[i]->get_voto() && deputadoVector[i]->get_cargo() == "DEPUTADO FEDERAL" && i != vdf)
                {
                        vdf = deputadoVector.size() - 1;
                }
        }

        for (unsigned int i = 0; i < deputadoVector.size(); i++)
        {
                if (deputadoVector[vg]->get_voto() == deputadoVector[i]->get_voto() && deputadoVector[i]->get_cargo() == "GOVERNADOR" && i != vg)
                {
                        vg = deputadoVector.size() - 1;
                }
        }

        for (unsigned int i = 0; i < candidatoVector.size(); i++)
        {
                if (candidatoVector[vencedor]->get_voto() == candidatoVector[i]->get_voto() && candidatoVector[i]->get_cargo() == "PRESIDENTE" && i != vencedor)
                {
                        vencedor = candidatoVector.size() - 1;
                }
        }

        for (unsigned int i = 0; i < deputadoVector.size(); i++)
        {
                if (deputadoVector[vs]->get_voto() == deputadoVector[i]->get_voto() && deputadoVector[i]->get_cargo() == "SENADOR" && i != vs)
                {
                        vs = deputadoVector.size() - 1;
                }
        }
        cout << "Vencedores: " << endl;
        cout << "Presidente: " << candidatoVector[vencedor]->get_nomeurna() << endl;
        cout << "Senador: " << deputadoVector[vs]->get_nomeurna() << endl;
        cout << "Governador: " << deputadoVector[vg]->get_nomeurna() << endl;
        cout << "Deputado Federal: " << deputadoVector[vdf]->get_nomeurna() << endl;
        cout << "Deputado Distrital: " << deputadoVector[vdd]->get_nomeurna() << endl;
        cout << "\n\n"
             << endl;
        ofstream relatorio("Relatorio.txt");
        relatorio << "Relatorio de votos\n\n"
                  << endl;
        for (unsigned int c = 0; c < vetor.size(); c++)
        {
                relatorio << "Nome: " << vetor[c]->get_nome() << endl;
                relatorio << "Presidente: " << vetor[c]->get_votoP() << endl;
                relatorio << "Senador: " << vetor[c]->get_votoS() << endl;
                relatorio << "Governador: " << vetor[c]->get_votoG() << endl;
                relatorio << "Deputado Federal: " << vetor[c]->get_votoDF() << endl;
                relatorio << "Deputado Distrital: " << vetor[c]->get_votoDE() << endl;
                relatorio << "\n"
                          << endl;
        }
}
