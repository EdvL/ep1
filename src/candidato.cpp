#include "candidato.hpp"

Candidato::Candidato(string cargo, string codigo, string nomeurna, string partido, int voto)
{
	set_codigo(codigo);
	set_partido(partido);
	set_nomeurna(nomeurna);
	set_cargo(cargo);
	set_voto(0);
}

Candidato::~Candidato() {}
string Candidato::get_codigo()
{
	return codigo;
}
void Candidato::set_codigo(string codigo)
{
	this->codigo = codigo;
}

string Candidato::get_partido()
{
	return partido;
}
void Candidato::set_partido(string partido)
{
	this->partido = partido;
}
string Candidato::get_nomeurna()
{
	return nomeurna;
}
void Candidato::set_nomeurna(string nomeurna)
{
	this->nomeurna = nomeurna;
}
string Candidato::get_cargo()
{
	return cargo;
}
void Candidato::set_cargo(string cargo)
{
	this->cargo = cargo;
}
int Candidato::get_voto()
{
	return voto;
}
void Candidato::set_voto(int voto)
{
	this->voto = voto;
}
