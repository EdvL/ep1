#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "candidato.hpp"
#include "eleitor.hpp"
#include "eleicao.hpp"
using namespace std;

int main()
{
        string votoP, votoS, votoG, votoDF, votoDD, name, num_presidente, num_senador, num_governador, num_depF, num_depD;
        string escolha, cont, nome, confb;
        vector<Eleitor *> eleitorVector;
        short int num_eleitores;
        Eleicao *eleicao_fon = new Eleicao;
        eleicao_fon->readCand();
        eleicao_fon->readDep();
        //-------------------Inicializando--Urna---------------------------------------
NUM:
        cout << "Insira o número de eleitores: " << endl;
        cin >> num_eleitores;
        
        while(cin.fail()){
                cin.clear();
                cin.ignore(1000, '\n');
                cout << "Insira o número de eleitores: "<< endl;;
                cin >> num_eleitores;
                cin.ignore(1000, '\n');
        }
       
        if (num_eleitores < 0)
        {
                cout << "Insira uma valor válido" << endl;
                goto NUM;
        }
        
        for (short int aux = 0; aux < num_eleitores; aux++)
        {
                cout << "Insira seu nome:" << endl;
                cin.ignore();
                getline(cin, nome);
               

        
                cout << "PRESIDENTE\n\n\n"
                     << endl;
        CHOICE:
                cout << "Para votar em branco, digite (B) para BRANCO\nPara escolher um candidato, digite (C) para CONTINUAR" << endl;
                cin >> escolha;
                if ((escolha != "B" && escolha != "b") && (escolha != "C" && escolha != "c"))
                {
                        cout << "Informe uma opção válida" << endl;
                        goto CHOICE;
                }
                if (escolha == "C" || escolha == "c")
                {
                        cout << "Insira o número do candidato (2 digitos): " << endl;
                        cin >> num_presidente;
                        eleicao_fon->infoCandidato(num_presidente, "PRESIDENTE");
                        cout << "\n"
                             << endl;
                CONF:
                        cout << "Para confirmar seu voto, digite (1) para CONFIRMAR\nPara cancelar e escolher outro candidato, digite (2) CORRIGE" << endl;
                        cin >> cont;
                        if (cont != "2" && cont != "1")
                        {
                                cout << "Insira uma opção válida" << endl;
                                goto CONF;
                        }
                        if (cont == "1")
                        {
                                votoP = num_presidente;
                                cout << "Voto confirmado!" << endl;
                        }
                        if (cont == "2")
                        {
                                cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                                     << endl;
                                goto CHOICE;
                        }
                }
                if (escolha == "B" || escolha == "b")
                {
                        CONFBP:cout << "Para confirmar o voto em branco, digite 1" << endl;
                        cout << "Para corrigir, digite 2" << endl;
                        cin >> confb;
                        if(confb == "2"){
                                goto CHOICE;
                        }
                        if(confb == "1"){
                        votoP = "BRANCO";
                        cout << "Voto em branco computado!" << endl;
                        }
                        if(confb != "1" && confb != "2"){
                                cout << "insira um valor válido";
                                goto CONFBP; 
                        }
                }
                cout << "\n\n\n\n\n\n\n"
                     << endl;
        SEN:
                cout << "SENADOR\n\n\n"
                     << endl;
        CHSEN:
                cout << "Para votar em branco, digite (B) BRANCO\nPara escolher um candidato, digite (C) CONTINUAR" << endl;
                cin >> escolha;
                if ((escolha != "B" && escolha != "b") && (escolha != "C" && escolha != "c"))
                {
                        cout << "Informe uma opção válida" << endl;
                        goto CHSEN;
                }

                if (escolha == "C" || escolha == "c")
                {
                        cout << "Insira o número do candidato (3 digitos): " << endl;
                        cin >> num_senador;
                        eleicao_fon->infoDep(num_senador, "SENADOR");
                        cout << "\n"
                             << endl;
                CONFS:
                        cout << "Para confirmar seu voto, digite (1) para CONFIRMAR\nPara cancelar e escolher outro candidato, digite (2) CORRIGE" << endl;
                        cin >> cont;
                        if (cont != "2" && cont != "1")
                        {
                                cout << "Insira uma opção válida" << endl;
                                goto CONFS;
                        }

                        if (cont == "1")
                        {
                                votoS = num_senador;
                                cout << "Voto confirmado!" << endl;
                        }
                        if (cont == "2")
                        {
                                cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                                     << endl;
                                goto SEN;
                        }
                }
                if (escolha == "B" || escolha == "b")
                {
                        cout << "Para confirmar o voto em branco, digite 1" << endl;
                        cout << "Para corrigir, digite 2" << endl;
                        cin >> confb;
                        if(confb == "2"){
                                goto CHOICE;
                        }
                        if(confb == "1"){
                        votoS = "BRANCO";
                        cout << "Voto em branco computado!" << endl;
                        }
                        if(confb != "1" && confb != "2"){
                                cout << "insira um valor válido";
                                goto CHSEN; 
                        }
                        
                }
                cout << "\n\n\n\n\n\n\n\n"
                     << endl;
        GOV:
                cout << "GOVERNADOR\n\n"
                     << endl;
        CHGOV:
                cout << "Para votar em branco, digite (B) para BRANCO\nPara escolher um candidato, digite (C) para CONTINUAR" << endl;
                cin >> escolha;
                if ((escolha != "B" && escolha != "b") && (escolha != "C" && escolha != "c"))
                {
                        cout << "Informe uma opção válida" << endl;
                        goto CHGOV;
                }

                if (escolha == "C" || escolha == "c")
                {
                        cout << "Insira o número do candidato (2 digitos): " << endl;
                        cin >> num_governador;
                        eleicao_fon->infoDep(num_governador, "GOVERNADOR");
                        cout << "\n"
                             << endl;
                CONFG:
                        cout << "Para confirmar seu voto, digite (1) para CONFIRMAR\nPara cancelar e escolher outro candidato, digite (2) CORRIGE" << endl;
                        cin >> cont;
                        if (cont != "2" && cont != "1")
                        {
                                cout << "Insira uma opção válida" << endl;
                                goto CONFG;
                        }

                        if (cont == "1")
                        {
                                votoG = num_governador;
                                cout << "Voto confirmado!" << endl;
                        }
                        if (cont == "2")
                        {
                                cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                                     << endl;
                                goto GOV;
                        }
                }
                if (escolha == "B" || escolha == "b")
                {
                        cout << "Para confirmar o voto em branco, digite 1" << endl;
                        cout << "Para corrigir, digite 2" << endl;
                        cin >> confb;
                        if(confb == "2"){
                                goto CHOICE;
                        }
                        if(confb == "1"){
                        votoG = "BRANCO";
                        cout << "Voto em branco computado!" << endl;
                        }
                        if(confb != "1" && confb != "2"){
                                cout << "insira um valor válido" << endl;
                                goto CHGOV; 
                        }
                       
                }
                cout << "\n\n\n\n\n\n\n\n"
                     << endl;
        DF:
                cout << "DEPUTADO FEDERAL\n\n"
                     << endl;
                cout << "Para votar em branco, digite (B) para BRANCO\nPara escolher um candidato, digite (C) para CONTINUAR" << endl;
                cin >> escolha;
                if ((escolha != "B" && escolha != "b") && (escolha != "C" && escolha != "c"))
                {
                        cout << "Informe uma opção válida" << endl;
                        goto DF;
                }

                if (escolha == "C" || escolha == "c")
                {
                        cout << "Insira o número do candidato (4 digitos): " << endl;
                        cin >> num_depF;
                        eleicao_fon->infoDep(num_depF, "DEPUTADO FEDERAL");
                        cout << "\n"
                             << endl;
                CONFDF:
                        cout << "Para confirmar seu voto, digite (1) para CONFIRMAR\nPara cancelar e escolher outro candidato, digite (2) CORRIGE" << endl;
                        cin >> cont;
                        if (cont != "2" && cont != "1")
                        {
                                cout << "Insira uma opção válida" << endl;
                                goto CONFDF;
                        }

                        if (cont == "1")
                        {
                                votoDF = num_depF;
                                cout << "Voto confirmado!" << endl;
                        }
                        if (cont == "2")
                        {
                                cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                                     << endl;
                                goto DF;
                        }
                }
                if (escolha == "B" || escolha == "b")
                {
                        cout << "Para confirmar o voto em branco, digite 1" << endl;
                        cout << "Para corrigir, digite 2" << endl;
                        cin >> confb;
                        if(confb == "2"){
                                goto CHOICE;
                        }
                        if(confb == "1"){
                        votoDF = "BRANCO";
                        cout << "Voto em branco computado!" << endl;
                        }
                        if(confb != "1" && confb != "2"){
                                cout << "insira um valor válido";
                                goto DF; 
                        }
                        
                }
                cout << "\n\n\n\n\n\n\n\n"
                     << endl;
        DD:
                cout << "DEPUTADO DISTRITAL\n\n"
                     << endl;
                cout << "Para votar em branco, digite (B) para  BRANCO\nPara escolher um candidato, digite (C) CONTINUAR" << endl;
                cin >> escolha;
                if ((escolha != "B" && escolha != "b") && (escolha != "C" && escolha != "c"))
                {
                        cout << "Informe uma opção válida" << endl;
                        goto DD;
                }

                if (escolha == "C" || escolha == "c")
                {
                        cout << "Insira o número do candidato (5 digitos): " << endl;
                        cin >> num_depD;
                        eleicao_fon->infoDep(num_depD, "DEPUTADO DISTRITAL");
                        cout << "\n"
                             << endl;
                CONFDD:
                        cout << "Para confirmar seu voto, digite (1) CONFIRMAR\nPara cancelar e escolher outro candidato, digite (2) CORRIGE" << endl;
                        cin >> cont;
                        if (cont != "2" && cont != "1")
                        {
                                cout << "Insira uma opção válida" << endl;
                                goto CONFDD;
                        }

                        if (cont == "1")
                        {
                                votoDD = num_depD;
                                cout << "Voto confirmado!" << endl;
                        }
                        if (cont == "2")
                        {
                                cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                                     << endl;
                                goto DD;
                        }
                }
                if (escolha == "B" || escolha == "b")
                {
                        cout << "Para confirmar o voto em branco, digite 1" << endl;
                        cout << "Para corrigir, digite 2 " << endl;
                        cin >> confb;
                        if(confb == "2"){
                                goto CHOICE;
                        }
                        if(confb == "1"){
                        votoDD = "BRANCO";
                        cout << "Voto em branco computado!" << endl;
                        }
                        if(confb != "1" && confb != "2"){
                                cout << "insira um valor válido";
                                goto DD; 
                        }
                        
                }
                cout << "Votos computados!" << endl;
                cout << "\n\n\n\n\n\n\n\n"
                     << endl;
                eleitorVector.push_back(new Eleitor(nome, votoP, votoS, votoG, votoDF, votoDD));
        }
        eleicao_fon->Apuracao(eleitorVector);
        cout << "Votacao concluida!" << endl;

        return 0;
}
