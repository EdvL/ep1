# Exercício de Programação 1
### Urna eletrônica

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)


Este projeto foi desenvolvido em **C++** e possui as funcionalidades básicas de uma urna eletrônica, incluindo BRANCO, CORRIGE E CONFIRMA.
## Como usar o projeto

* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```
##### Insira o número de eleitores que participarão

![step1](/uploads/4cae73958931cdc621f9ca2fb0e7da8b/step1.png)

##### Insira seu nome completo

![step2](/uploads/e6af1099ac09b2fe18d6a3b0cf9a8cbd/step2.png)

##### Verfique o cargo que está na tela e escolha entre votar em branco digitando B ou b. Para continuar e escolher um candidato, digite C ou c
* Este passo ocorrerá em todos os cargos

![step3](/uploads/751a11d5deb9f3d4def0254db416e4bc/step3.png)

##### Insira o número de seu candidato e verifique as informações do mesmo. Para confirmar o voto, digite 1, para corrigir digite 2
* Este passo ocorrerá em todos os cargos
 

![step4](/uploads/f47a3c460d43376b71af80c79045185e/step4.png)

##### Após todos os eleitores votarem, o resultado da eleição será exibido na tela

![step5](/uploads/248d3a8273096a9776ee62f2dd31fdfc/step5.png)

##### Um relatório em formato .txt será emitido na pasta principal do projeto, contendo as informações dos eleitores e voto de cada

![Relatorioout](/uploads/1dc1ea77b5ef87e22294841eec416e4f/Relatorioout.png)
![RelatorioIN](/uploads/cdcacd7badabbb039254faf872a35cfa/RelatorioIN.png)


## Bugs e problemas

Caso o usuário não corrija o voto em caso de candidato inexistente, o resultado será correto.
Porém o relatório não será preciso no cargo em que este voto ocorreu.
O restante dos cargos estarão corretos.




