#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP
#include <string>
using namespace std;

class Candidato
{
  protected:
    string codigo;
    string partido;
    string nomeurna;
    string cargo;
    int voto;

  public:
    Candidato(string, string, string, string, int);
    ~Candidato();
    string get_codigo();
    void set_codigo(string);
    string get_partido();
    void set_partido(string);
    string get_nomeurna();
    void set_nomeurna(string);
    string get_cargo();
    void set_cargo(string);
    int get_voto();
    void set_voto(int);
};
#endif
