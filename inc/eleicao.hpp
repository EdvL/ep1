#ifndef ELEICAO_HPP
#define ELEICAO_HPP
#include "candidato.hpp"
#include <vector>
#include "eleitor.hpp"
using namespace std;
class Eleicao
{

  public:
	string cargoV;
	string codigoV;
	string nomeurnaV;
	string partidoV;
	int votoV;
	//=========================================
	vector<Candidato *> candidatoVector;
	vector<Candidato *> deputadoVector;
	void infoCandidato(string, string);
	void Apuracao(vector<Eleitor *> &);
	void infoDep(string, string);
	void readCand();
	void readDep();
};

#endif
