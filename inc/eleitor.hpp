#ifndef ELEITOR_HPP
#define ELEITOR_HPP
#include <string>

using namespace std;

class Eleitor
{
  protected:
	string nome;
	string votoP;
	string votoS;
	string votoG;
	string votoDF;
	string votoDE;

  public:
	Eleitor(string, string, string, string, string, string);
	~Eleitor();
	void set_nome(string);
	string get_nome();
	void set_votoP(string);
	string get_votoP();
	void set_votoS(string);
	string get_votoS();
	void set_votoG(string);
	string get_votoG();
	void set_votoDF(string);
	string get_votoDF();
	void set_votoDE(string);
	string get_votoDE();
};

#endif
